package com.ct.safevargs;

import java.util.ArrayList;
import java.util.List;

public class SafeVarargsChanges {
	
	@SafeVarargs
	private void print(List... names) {  
        for (List<String> name : names) {  
            System.out.println(name);  
        }  
    }  
    public static void main(String[] args) {  
    	SafeVarargsChanges obj = new SafeVarargsChanges();  
        List<String> list = new ArrayList<String>();  
        list.add("Kevin");  
        list.add("Rick"); 
        list.add("Negan");
        obj.print(list);  
    }     

}
