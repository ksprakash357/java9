package com.ct.collecction_map_changes;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionTests {
	
	/* Factory methods for Collections
	 * Factory methods are static methods used to create immutable instances of
	 * collections. It is unmodifiable, therefore adding new element will throw
	 * java.lang.UnsupportedOperationException
	 */
	
	//before java 9

 /*   public static void main(String args[]) {
 
        //Before Java 9 - Creating List
        List <String> list = new ArrayList <> ();
 
        list.add("cl");
        list.add("one");
        list.add("tab");
 
        list = Collections.unmodifiableList(list);
        System.out.println("List values are: " + list);
        
      	//Before Java 9 - Creating Set
       	Set <String> set = new HashSet <> ();
        set.add("cl");
        set.add("one");
        set.add("tab");
        set = Collections.unmodifiableSet(set);
        System.out.println("Set values are: " + set);
        
        //Before Java 9 - Creating Map
 		  
 		Map <String, String> map = new HashMap <> ();
        map.put("1", "cl");
        map.put("2", "one");
        map.put("3", "tab");
        map = Collections.unmodifiableMap(map);
        System.out.println("Map values are: " + map);

	*/
	
	public static void main(String[] args) {
		 
        //Java 9 - Creating List
        List <String> list = List.of("cl", "one", "tab");
        
        //Java 9 - Creating Set
        Set <String> set = Set.of("cl", "one", "tab");
 
        //Java 9 - Creating Map
        Map <String, String> map = Map.of("1", "cl", "2", "one", "3", "tab");
        
        //Java 7 - Printing Set
        System.out.println("Printing list using traditional for loop:\n");
        for (String s: list) {
            System.out.println(s);
        }
 
        //Java 8 - Printing Set
        System.out.println("\nPrinting list using forEach:\n");
        list.stream().forEach(s -> System.out.println(s));
        
        
        System.out.println("\nPrinting set using forEach:\n");
        set.stream().forEach(s -> System.out.println(s));
   
        System.out.println("\nPrint map using forEach:\n");
        map.forEach((k, v) -> System.out.println(k + " : " + v));
 
   
     /**   One of the enhancements Oracle has made in Java 9 is adding syntactic sugar for creating immutable maps.   
           By immutable, we mean it cannot change - no adding or removing entries is permitted.  
	*/
	
	
	
	
}
}
