package com.ct.newHtml5doc;


/**
 * @author clonetab
 * @version 2.O
 */

public class JavaDocsChanges {
	
	/*
	 * Java documentation can be generated using javadoc tool. It currently
	 * generates documentation in html 4.0 format. In java 9,we can generate
	 * documentation in html 5 format by using -html5 option in command line
	 * arguments.
	 */

//	by using this cmd in java 9 i genarated doc files < javadoc -d -html5 JavaDocs.java >
	
	
	   public static void main(String []args) {
	      System.out.println("Hello Clonetab World");
	   }
	}

