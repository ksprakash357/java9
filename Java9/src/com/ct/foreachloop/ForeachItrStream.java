package com.ct.foreachloop;

import java.util.ArrayList;
import java.util.List;

public class ForeachItrStream {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("cl");
		list.add("one");
		list.add("tab");
		list.add("pvt");
		list.add("ltd");
		
		list.add("cl");
		list.add("one");
		list.add("tab");
	
//		creating stream 
		list.stream()
//		filtering names that starts with t
		.filter(f -> f.startsWith("t")).forEach(n -> System.out.println(n));
		
		
		 
	}
}
