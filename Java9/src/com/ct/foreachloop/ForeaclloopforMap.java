package com.ct.foreachloop;

import java.util.HashMap;
import java.util.Map;

public class ForeaclloopforMap {

	public static void main(String[] args) {
	      Map<Integer, String> hmap = new HashMap<Integer, String>();
	      hmap.put(1, "cl");
	      hmap.put(2, "one"); 
	      hmap.put(3, "tab");  
	      hmap.put(4, "soft");   
	      hmap.put(5, "pvt");   
	      hmap.put(6, "ltd");
	      
	      /* forEach to iterate and display each key and value pair
	       * of HashMap.    
	       */  
	      hmap.forEach((key,value)->System.out.println(key+" - "+value));
	      
	      
	      /* forEach to iterate a Map and display the value of a particular  
	       * key     
	       */ 
	      hmap.forEach((key,value)->{ 
	         if(key == 4){ 
	            System.out.println("Value associated with key 4 is: "+value); 
	         }  
	      });   
	      
	      
	      /* forEach to iterate a Map and display the key associated with a
	       * particular value     
	       */
	      hmap.forEach((key,value)->{
	         if("one".equals(value)){ 
	            System.out.println("Key associated with Value one is: "+key);
	         }
	      }); 
	   }
}
