package com.ct.processapichanges;

public class ProcessAPI {


/*In Java 9 Process API which is responsible to control and manage operating system processes has been improved considerably. 
ProcessHandle Class now provides process's native process ID, start time, accumulated CPU time, arguments, command, user, 
parent process, and descendants.

ProcessHandle class also provides method to check processes' liveness and to destroy processes. 
It has onExit method, the CompletableFuture class can perform action asynchronously when process exits.

*/
	
	public static void main(String[] args) {

		System.out.println("Your Process id is :" + ProcessHandle.current().pid());
	}

}
