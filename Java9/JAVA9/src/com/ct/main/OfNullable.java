package com.ct.main;

import java.util.stream.Stream;

/*
Java 9 has added ofNullable() method to Stream API.It is introduced to prevent NullPointerExceptions.
It returns sequential Stream containing single element.If element is null then return empty stream.

The main objective is to return empty Optionals if the value is null.

*/

public class OfNullable {
	
	  public static void main(String[] args) {
		  
		  
	   
		  System.out.println("\nofNullable Example 1");
		  long count = Stream.ofNullable(5000).count();
		  System.out.println(count);
		 
		  System.out.println("\nofNullable Example 2");
		  count = Stream.ofNullable(null).count();
		  System.out.println(count);
		  
		  
	    
	   } 
	}