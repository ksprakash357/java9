package com.ct.underscr_changes;

public class UnderscoreChanges {
	
	
	
	/*
	 * Before Java SE 8 release, we can use Underscore in identifiers. And also we
	 * can use Underscore alone as an identifier without any issues. Even though it
	 * is not recommended, but it works fine without any issues.
	 */
	
	
	//in java 7
	/*
	 * public static void main(String[] args) { String _ = "clonetab";
	 * System.out.println("Value of underscore (_) = " + _); }
	 * o/p: Value of underscore (_) clonetab
	 */
	
	//in java 9
	
	//Oracle Corp is going to remove that underscore (�_�) usage as identifier completely in Java SE 9 release.
	
  /**	public static void main(String[] args) { 
		
		String _ = "clonetab";
	
		System.out.println("Value of underscore (_) = " + _);
	 }
	*\
	
	
	
	
	/*
	 * O/P
	 * 
	 * String _="clonetab"; | Error: | as of release 9, '_' is a keyword, and may
	 * not be used as an identifier | String _="clonetab";
	 */
	
}
