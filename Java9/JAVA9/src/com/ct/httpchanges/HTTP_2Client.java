package com.ct.httpchanges;

import java.net.URI;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;


/*A new HTTP Client implements HTTP/2 and WebSocket, and replace legacy the legacy HttpURLConnection API.
The API will be delivered as an incubator module. It means the API isn�t guaranteed to be 100% final yet.

The API will live under the jdk.incubtor namespace. As existing or Legacy HTTP Client API has numerous issues 
(like supports HTTP/1.1 protocol and does not support HTTP/2 protocol and WebSocket,
		works only in Blocking mode and a lot of performance issues).
		*/

public class HTTP_2Client {

	public static void main(String[] args) throws Exception {

		HttpClient client = HttpClient.newHttpClient();

		HttpRequest req = HttpRequest.newBuilder(URI.create("https://www.codenuclear.com")).header("User-Agent", "Java")
				.GET().build();

		HttpResponse<String> resp = client.send(req, HttpResponse.BodyHandler.asString());
	}

}
