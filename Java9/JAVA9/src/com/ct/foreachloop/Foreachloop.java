package com.ct.foreachloop;

import java.util.ArrayList;
import java.util.List;

public class Foreachloop {
	
/**	Java provides a new method forEach() to iterate the elements. It is defined in Iterable and Stream interface. 
	It is a default method defined in the Iterable interface. Collection classes which extends Iterable interface
	can use forEach loop to iterate elements.
	This method takes a single parameter which is a functional interface. So, you can pass lambda expression as an argument.
	*/
	
	  public static void main(String[] args) {  
	        List<String> list = new ArrayList<String>();  
	        list.add("cl");
	        list.add("one");
	        list.add("tab");
	        list.add("pvt");
	        list.add("ltd");
	        
	        
//	        Old Method
	        
	 /*       for (String name : names) {
	            System.out.println(name);
	        }
	        
	     */   
	        
//	        Iterating by passing lambda expression 
	        
	        list.forEach(listpri->System.out.print(listpri));
	        
//	        Iterating by passing method reference
	        
	        list.forEach(System.out::println);
	         
	        
	    }  

}
