package com.ct.innerclasschanges;


abstract class MyClass<T>{  
	abstract T add(T num, T num2);  
}  

public class Diamondoperator_InnerClass {


	/*	Java 7 allowed us to use diamond operator in normal classes 
	but it didn�t allow us to use them in anonymous inner classes.
	We got a compilation error when we ran the code in Java SE 8.
	 */

	/*	
	 * abstract class MyClass<T>{  
	    abstract T add(T num, T num2);  
	}  
	public class JavaExample {  
	    public static void main(String[] args) {  
	        MyClass<Integer> obj = new MyClass<>() {  
	            Integer add(Integer x, Integer y) {  
	                return x+y;   
	            }  
	        };    
	        Integer sum = obj.add(100,101);  
	        System.out.println(sum);  
	    }  
	}

	 *
	 */


	public static void main(String[] args) {  
		MyClass<Integer> obj = new MyClass<>() {  
			Integer add(Integer x, Integer y) {  
				return x+y;   
			}  
		};    
		Integer sum = obj.add(100,101);  
		System.out.println(sum);  
	}  
}




