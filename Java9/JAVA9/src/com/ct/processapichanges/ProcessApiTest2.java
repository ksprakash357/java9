package com.ct.processapichanges;

import java.io.IOException;
import java.time.ZoneId;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProcessApiTest2 {
	
	/*
	 * Prior to Java 5, the only way to spawn a new process was to use the
	 * Runtime.getRuntime().exec() method. Then in Java 5, ProcessBuilder API was
	 * introduced which supported a cleaner way of spawning new processes. Now Java
	 * 9 is adding a new way of getting information about current and any spawned
	 * process.
	 */
	
	/*
	 * To get information of any process, now you should use
	 * java.lang.ProcessHandle.Info interface. This interface can be useful in
	 * getting lots of information
	 */
	
	
	
	public static void main(String[] args) throws IOException {
		ProcessBuilder pb = new ProcessBuilder("notepad.exe");
		String np = "Not Present";
		Process p = pb.start();
		ProcessHandle.Info info = p.info();
		System.out.printf("Process ID : %s%n", p.pid());
		System.out.printf("Command name : %s%n", info.command().orElse(np));
		System.out.printf("Command line : %s%n", info.commandLine().orElse(np));

		System.out.printf("Start time: %s%n",
				info.startInstant().map(i -> i.atZone(ZoneId.systemDefault()).toLocalDateTime().toString()).orElse(np));

		System.out.printf("Arguments : %s%n",
				info.arguments().map(a -> Stream.of(a).collect(Collectors.joining(" "))).orElse(np));

		System.out.printf("User : %s%n", info.user().orElse(np));
	}

}