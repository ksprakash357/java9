package com.ct.tryresourcechanges;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class JavatrywithresourceChanges {

	/*
	 * There were certain issues with the Try-With-Resource statement in Java 7.
	 * This statement didn�t allow the resources to be declared outside of the
	 * statement block (scope). Lets take an example to understand this.
	 * 
	 */

	// in java 7
	
	/* public static void main(String[] args) throws FileNotFoundException { 
	       FileOutputStream fileOutputStream = new FileOutputStream("beginnersbook.txt");
	       try(FileOutputStream fileOutputStream2 = fileOutputStream){ 
	            String mystring = "We are writing this line in the output file."; 
	            byte bytes[] = mystring.getBytes();       
	            fileOutputStream2.write(bytes);      
	            System.out.println("The given String is written in the file successfully");           
	        }catch(Exception e) {  
	            System.out.println(e);  
	        }         
	    } 
	
	**/
	/**
	 * O/P : Compile-time error
			The above example throws compile-time error because the resource is declared outside 
			the scope of Try-With-Resource statement.
	 */
	
	// now i tested in java 9
	
/**	Java 9 provides additional flexibility while using try-with-resource blocks,
	now we don't have the restriction of declaring each closeable object inside try parentheses anymore,
	we just need to use their references:
*/
	public static void main(String[] args) throws FileNotFoundException {
		
		FileOutputStream fileOutputStream = new FileOutputStream("ctsiva.txt");
		try (fileOutputStream) {
			String mystring = "We are writing this line in the output file.";
			byte bytes[] = mystring.getBytes();
			fileOutputStream.write(bytes);
			System.out.println("The given String is written in the file successfully");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
