package com.ct.interfachanges;

public interface Java9 {
	
	/*In Java 9, we can create private methods inside an interface.
	  Such like, we can also create private static methods inside an interface
	Interface allows us to declare private methods that help to share common code between non-abstract methods.

	Before Java 9, creating private methods inside an interface cause a compile time error. 
	The following example is compiled using Java 8 compiler and throws a compile time error.
	*/
	

	default void metod() {
		privatetest();
		privastatictetest();
	}
    // Private method inside interface  
    private void privatetest() {  
        System.out.println("I'm private method");  
    }  
    // Private static method inside interface  
    private static void privastatictetest() {  
        System.out.println("I'm private static method");  
    }  
} 
	
	
	

